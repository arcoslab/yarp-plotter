#!/usr/bin/env python

import sys
import signal as sigterm
import pygame
import optparse
import yarp
from avispy.engine import Camera, Scene, Light, Display
from avispy.objects_lib import Disk, Curve, Plot
import OpenGL.GL as gl
from numpy import array
from numpy.linalg import inv
from arcospyu.control.control_loop import Controlloop
from builtins import range


def configParse(argv):
    optparse.SUPPRESS_HELP
    parser = optparse.OptionParser(
        "usage: %prog [options]", add_help_option=False)
    parser.add_option("-e", "--help", action="help")
    parser.add_option("-w", "--width", dest="width", default="720",
                      type="int", help="Width")
    parser.add_option("-h", "--height", dest="height", default="480",
                      type="int", help="Height")
    parser.add_option("--capture_dir", dest="capture_dir", default="./",
                      type="string", help="capture directory")
    parser.add_option("--portbasename", dest="portbasename",
                      default="yarp-plotter", type="string",
                      help="Yarp port base name")
    parser.add_option("-c", action="store_true", dest="capture", default=False,
                      help="Enable image capture")
    (options, args) = parser.parse_args(argv[1:])
    return (options, args)


def terminate_handler(signum, stack_frame):
    print("Catched signal", signum)
    yarp.Network.fini()
    sys.exit()


class My_loop(Controlloop):
    def __init__(self, *args, **kwargs):
        super(My_loop, self).__init__(*args, **kwargs)
        self.counter = 0
        self.colors = [[1.0, 0.0, 0.0],
                       [0.0, 1.0, 0.0],
                       [0.1, 0.1, 1.0],
                       [1.0, 0.5, 0.0],
                       [0.0, 1.0, 0.5],
                       [0.3, 0.0, 1.0]]
        self.current_color = 0

        options, args = configParse(sys.argv)

        self.camera = Camera()
        size = options.width, options.height
        self.scene = Scene()
        if options.capture:
            self.display = Display(self.camera, self.scene, res=size,
                                   image_dump_dir=options.capture_dir)
        else:
            self.display = Display(self.camera, self.scene, res=size,
                                   image_dump_dir="")

        light0 = Light(gl.GL_LIGHT0)
        light0.position = array([10., 10., 10., 1.0])
        self.scene.add_light(light0)

        light1 = Light(Light.LIGHTS[1])
        light1.position = array([-10., 10., 10., 1.0])
        self.scene.add_light(light1)

        light2 = Light(Light.LIGHTS[2])
        light2.position = array([0., -10., 10., 1.0])
        self.scene.add_light(light2)

        light3 = Light(Light.LIGHTS[3])
        light3.position = array([0., 0., -10., 1.0])
        self.scene.add_light(light3)

        # camera center object
        self.camera_center = Disk()
        self.camera_center.set_color(array([0.5, 0.5, 0.5]))
        self.camera_center.set_color_reflex(array([1., 1., 1.]), 50.0)
        self.camera_center.visibility = False
        self.scene.add_object(self.camera_center)

        curve_first = Curve()
        curve_first.set_color(array([1., 0., 0.]))
        curve_first.set_color_reflex(array([1., 0., 0.]), shininess=0.0)

        self.plot = Plot()
        self.plot.set_pos(array([-6.3, -5.6, 0.]))
        self.plot.set_color(array([0.3, 0.3, 0.3]))
        self.plot.set_color_reflex(array([0.3, 0.3, 0.3]), shininess=0.0)
        self.plot.set_x_div(5.0)
        self.plot.set_y_div(40.0)
        self.plot.scale = [14., 14., 1.0]
        self.scene.add_object(self.plot)

        yarpbaseportname = "/"+options.portbasename+"/yarp-plotter"
        yarp.Network.init()

        # signals port
        self.signals_port = yarp.BufferedPortBottle()
        self.signals_port.open(yarpbaseportname+"/signals")

        self.curves = []

    def create_extra_curves(self, extra_curves):
        print("Create "+str(extra_curves)+" more curves")
        for i in range(extra_curves):
            self.curves.append(Curve())
            self.curves[-1].set_color(
                array(self.colors[self.current_color]))
            self.curves[-1].set_color_reflex(
                array(self.colors[self.current_color]),
                shininess=0.0)
            self.current_color += 1
            if self.current_color >= len(self.colors):
                self.current_color = 0
            self.plot.add_curve(self.curves[-1])

    def handle_curves(self):
        # This is the efficiency bottleneck
        while self.signals_port.getPendingReads() != 0:
            signals_bottle = self.signals_port.read(False)
            if signals_bottle:
                signals = array(
                    list(
                        map(yarp.Value.asDouble,
                            map(signals_bottle.get,
                                range(signals_bottle.size())))))

                extra_curves = len(signals)-len(self.curves)
                if extra_curves > 0:
                    self.create_extra_curves(extra_curves)
                for signal, curve in zip(signals, self.curves):
                    curve.add_point(array([self.counter, signal, 0.0]))
                self.plot.gen_gl_list()

    def handle_pygame(self):
        # This could be the efficiency bottleneck
        for event in pygame.event.get():
            if event.type == pygame.QUIT or\
               (event.type == pygame.KEYDOWN and event.key == pygame.K_q):
                terminate_handler(sigterm.SIGTERM, None)
            # Camera events
            if event.type == pygame.MOUSEMOTION or \
               event.type == pygame.MOUSEBUTTONDOWN or \
               event.type == pygame.MOUSEBUTTONUP:
                self.camera.update(event)
                # TODO: the following is done twice innecessarly
                self.camera.camera_matrix.get_frame()
                val = self.camera.camera_matrix.radius*0.01
                self.camera_center.set_pos(
                    inv(self.camera.camera_matrix.center_rot_frame)[:, 3][:3])
                self.camera_center.scale = [val, val, 1.0]
                if event.type == pygame.MOUSEMOTION:
                    if event.buttons ==\
                       (1, 0, 0) or event.buttons == (0, 0, 1):
                        self.camera_center.visibility = True
                if event.type == pygame.MOUSEBUTTONDOWN or event.type ==\
                   pygame.MOUSEBUTTONUP:
                    if event.button == 4 or event.button == 5:
                        self.camera_center.visibility = True

    def process(self):
        self.counter += 0.4
        self.handle_pygame()
        # objects port processing
        self.handle_curves()
        self.display.update()
        self.camera_center.visibility = False


sigterm.signal(sigterm.SIGTERM, terminate_handler)
sigterm.signal(sigterm.SIGINT, terminate_handler)

loop = My_loop(50.)
loop.loop()
