#!/usr/bin/env python

from distutils.core import setup


setup(name='yarp-plotter',
      version='0.1',
      description='Python Yarp Signal Visualization Tool',
      author='Federico Ruiz Ugalde',
      author_email='memeruiz@gmail.com',
      url='http://www.arcoslab.org/',
      package_dir={'yarp-plotter': ''},
      packages=['yarp-plotter'],
      scripts=['yarp-plotter']
     )
